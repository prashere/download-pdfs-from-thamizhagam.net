import asyncio
import logging
import socket

from configparser import ConfigParser

from aiohttp import ClientSession, TCPConnector, ClientTimeout
from bs4 import BeautifulSoup

def get_logger(name, level=logging.INFO):
    _logger = logging.getLogger(name)
    if not _logger.level:
        _logger.setLevel(level)
    if not _logger.hasHandlers():
        default_handler = logging.StreamHandler()
        default_handler.setFormatter(logging.Formatter("[%(asctime)s] %(module)s: [%(levelname)s] %(message)s"))
        _logger.addHandler(default_handler)
    return _logger

def mark_finished(entry):
    with open('finished.txt', 'a') as fd:
        file_name = get_name_from_url(entry)
        log.info(f"[{file_name}]: DONE")
        fd.write(entry + "\n")

def get_name_from_url(url):
    file_name = url.split("/")[-1]
    return file_name

async def save_to_file(path, response):
    file_name = get_name_from_url(response.real_url.path)
    log.info(f"[{file_name}]: downloading...")
    with open(path+"/"+file_name, "wb") as fd:
        while True:
            chunk = await response.content.read(2048)
            if not chunk:
                break
            fd.write(chunk)

async def download(url, session, path=None):
    async with session.get(url) as response:
        log.debug(f"Response Status: {response.status}")
        if response.status == 200:
            if response.content_type == "application/pdf":
                await save_to_file(path, response)
                mark_finished(url)
            else:
                return await response.read()
        elif response.status >= 400:
            log.debug(f"Response Status: {response.status}")
            log.info(f"FAILED: {url}")

def get_list_of_links(host, anchors):
    return [host+a.get('href') for a in anchors]

def get_all_anchors(soup):
    return soup.find_all("a")

def get_first_table(soup):
    return soup.find_all("table")[0]

async def main(loop, config):
    HOST = config.get('site', 'host')
    INDEX_URL = config.get('site', 'index_path')
    LANGUAGE = config.get('site', 'language')
    SECTION = config.get('site', 'section')

    LIMIT = config.getint('app', 'concurrent_limit')
    DOWNLOADS_DIR = config.get('app', 'downloads_dir')
    FINISHED_FILE = config.get('app', 'finished')

    index_url = HOST+INDEX_URL+LANGUAGE+SECTION

    timeout = ClientTimeout(total=None)

    connector = TCPConnector(family=socket.AF_INET, limit=LIMIT)
    async with ClientSession(loop=loop, connector=connector, timeout=timeout) as session:
        book_index = await download(index_url, session)
        soup = BeautifulSoup(book_index, "html.parser")
        links = get_list_of_links(HOST, get_all_anchors(get_first_table(soup)))

        total_files = len(links)
        log.info(f"{total_files} to download.")

        with open(FINISHED_FILE, 'r') as fd:
            files_to_skip = [url.strip() for url in fd]
            log.info (f"Skipping {len(files_to_skip)} files.")

        if total_files:
            download_files = [loop.create_task(download(url, session, path=DOWNLOADS_DIR)) for url in links if url not in files_to_skip]
            log.debug(f"Files to Download: {len(download_files)}")
            await asyncio.gather(*download_files)
    await connector.close()


if __name__ == "__main__":
    log = get_logger(__name__)
    config = ConfigParser()
    config.read('config.ini')

    DEBUG = config.get('app', 'debug')
    if DEBUG:
        log.setLevel(logging.DEBUG)
        log.debug("debug mode is enabled")

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop=loop, config=config))
