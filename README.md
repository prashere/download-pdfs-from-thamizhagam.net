### Download PDFs From Thamizhagam.net

[Thamizhagam.net](http://thamizhagam.net) is a Tamil website where Ambedkar, Periyar, Marx, Lenin, Mao and other left text books are scanned and available for download as PDF files. This project contains a python script `fetch.py` that downloads the pdf files from the website given the section as per `config.ini` file.

The number of files to download concurrently at given time can also be configured via `concurrent_limit` in the `config.ini` file. The downloaded files are marked as finished and you can stop the program and resume any time so that the download will begin from the rest of the files to be downloaded.

It is written to run with python3.x + asyncio + aiohttp.

**Licensed under AGPL**
